<?php
session_start();
if ((isset($_SESSION['online'])) && ($_SESSION['online']==true))
{
  header('Location: ind.php');
  exit();
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
<title>helper - załóż darmowe konto!</title>
<?php include "php/shape/header.php"; ?>

</head>

 <body>

<script src="http://code.jquery.com/jquery-1.8.0.min.js"></script>

  <?php include "php/shape/l_menu.php"; ?>
  <content id="login">
      <p>Utwórz konto!</p><br />
      <form method="post" action="php/registration/registratio.php">
       <div class="val"> Login: <br /> <input type="text" name="login" pattern="[A-Za-z0-9]{3,16}" required /><br />

        <?php
          if (isset($_SESSION['e_login']))
          {
            echo '<div class="error">'.$_SESSION['e_login'].'</div>';
            unset($_SESSION['e_login']);
          }

        ?>
      </div>

        <div class="val">E-mail: <br /> <input type="email" value="
        <?php
          if(isset($_SESSION['fr_email']))
          {
            echo $_SESSION['fr_email'];
            unset($_SESSION['fr_email']);
          }
        ?>" name="email" required /><br />

        <?php
          if (isset($_SESSION['e_email']))
          {
            echo '<div class="error">'.$_SESSION['e_email'].'</div>';
            unset($_SESSION['e_email']);
          }

        ?>
</div>

       <div class="val"> Twoje hasło: <br /> <input type="password" name="password1" pattern="(?=^.{6,}$)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" required/><br />

        <?php
          if (isset($_SESSION['e_password']))
          {
            echo '<div class="error">'.$_SESSION['e_password'].'</div>';
            unset($_SESSION['e_password']);
          }

        ?>
      </div>

       <div class="val"> Powtórz hasło: <br /> <input type="password" name="password2" required/><br /><br />
       </div>
       <div class="val">
        <label>
        <input type="checkbox" name="regulamin" 
        <?php
        if(isset($_SESSION['fr_regulamin']))
          {
            echo "checked";
            unset($_SESSION['fr_regulamin']);
          }
        ?>  required/> Akceptuję <a href="#">regulamin</a>
        </label><br />
        <?php
            if (isset($_SESSION['e_regulamin']))
            {
              echo '<div class="error">'.$_SESSION['e_regulamin'].'</div>';
              unset($_SESSION['e_regulamin']);
            }

        ?></div><br />
        <div class="text-xs-center">
          <div class="g-recaptcha" data-sitekey="6Ldrq0AUAAAAAHwpWESyUEedJalImHQC4Ab2LlKv"></div>
        </div>
          <br /> 
        <?php
            if (isset($_SESSION['e_bot']))
            {
              echo '<div class="error">'.$_SESSION['e_bot'].'</div>';
              unset($_SESSION['e_bot']);
            }

          ?>
         <div class="val"> <input type="submit" value="Zarejestruj się" class="button_1"/>
          </div>
        </form>
        <br /><br />
       <p> <a href="log.php" class="links">powrót do logowania</a></p>
</content>
<?php include "php/shape/footer.html"; ?>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <?php include "php/shape/js.php"; ?>
    <script type="text/javascript" src="js/validation.js"></script>
 </body>

</html>