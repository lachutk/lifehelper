<?php
session_start();

$ip = $_SERVER['REMOTE_ADDR'];


if(isset($_POST['login']))
{

    // udana walidacja
    $its_ok=true;

    //poprawnosc name
    $login = $_POST['login'];

    //sprawdzenie loginu
  if(strlen($login)<3 || (strlen($login)>20))
  {
      $its_ok=false;
      $_SESSION['e_login']="Login musi posiadać od 3 do 20 znaków!";
      header('Location: ../../registration.php');
  }

  if(ctype_alnum($login)==false)
 {
      $its_ok=false;
      $_SESSION['e_login']="Login może składać się tylko z liter i cyfr (bez polskich znaków)";
      header('Location: ../../registration.php');
 }
   //sprawdzanie adresu email
  $email=$_POST['email'];
  $emailb=filter_var($email, FILTER_SANITIZE_EMAIL);

  if((filter_var($emailb, FILTER_VALIDATE_EMAIL)==false) || ($emailb!=$email))
  {
      $its_ok=false;
      $_SESSION['e_email']="Podaj poprawny adres e-mail!";
      header('Location: ../../registration.php');
  }

  //sprawdzanie hasła
  $password1=$_POST['password1'];
  $password2=$_POST['password2'];

  if((strlen($password1)<8) || (strlen($password1)>20))
  {
      $its_ok=false;
      $_SESSION['e_password']="Hasło musi posiadać od 8 do 20 znaków!";
      header('Location: ../../registration.php');
  }

  if($password1!=$password2)
  {
        $its_ok=false;
      $_SESSION['e_password']="Podane hasła nie są identyczne!";
      header('Location: ../../registration.php');
  }

  $password_hash = sha1($password1);

  //sprawdzanie regulaminu
  if(!isset($_POST['regulamin']))
   {
      $its_ok=false;
      $_SESSION['e_regulamin']="Potwierdź akceptację regulaminu!";
      header('Location: ../../registration.php');
   }

  //bot or not?
  $secret = "6Ldrq0AUAAAAAPxn1EL5irTwTT_BSChy0ckdnQHQ";

  $check=file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);

  $answer=json_decode($check); // json jakas odmiana jsa, która jest lekka

  if($answer->success==false)
  {
    $its_ok=false;
    $_SESSION['e_bot']="Potwierdź, że nie jesteś botem!";
    header('Location: ../../registration.php');
  }

//zapamiętywanie wprowadzonych poprawnych danych
  $_SESSION['fr_login']=$login;
  $_SESSION['fr_email']=$email;
  if(isset($_POST['regulamin'])) $_SESSION['fr_regulamin']=true;

  require_once "../database/connect.php";
  mysqli_report(MYSQLI_REPORT_STRICT); // sposoób raportowania wyjątków, zamiast ostrzeżeń bierz wyjątki

  try 
  {
      $connect = new mysqli($host, $db_user, $db_password, $db_name);
      if($connect->connect_errno!=0)
      {
        throw new Exception(mysqli_connect_errno()); //rzuć nowym wyjątkiem
      }
    else
      {
      // czy email istnieje?
        $result=$connect->query("SELECT id FROM users WHERE email='$email'");

        if(!$result) throw new Exception($connect->error);

        $how_many_mails=$result->num_rows;
        if($how_many_mails>0)
        {
          $its_ok=false;
          $_SESSION['e_email']="Istnieje już konto przypisane do tego adresu email!";
        }

        // czy login istnieje?
        $result=$connect->query("SELECT id FROM users WHERE user='$login'");

        if(!$result) throw new Exception($connect->error);

        $how_many_logins=$result->num_rows;
        if($how_many_logins>0)
        {
          $its_ok=false;
          $_SESSION['e_login']="Istnieje już konto o takim loginie! Wybierz inny.";
        }
        if($its_ok==true) // wszystkie testy ok
        {
          unset($_SESSION['e_login']);
          unset($_SESSION['e_email']);
          unset($_SESSION['e_bot']);
          unset($_SESSION['e_regulamin']);
          unset($_SESSION['e_password']);
          if($connect->query("INSERT INTO users (user, password, email, date_of_registration, ip) VALUES ('$login', '$password_hash', '$email', now(), '$ip')"))
          {
            $_SESSION['registeraccepted']=true;
            header('Location: index.php');
          }
          else
          {
            throw new Exception($connect->error);
          }
        }
        $connect->close();
      }
  }
  catch(Exception $e) //złap wyjątki, jeśli jakieś zostały rzucone
  {
    echo '<center><br /><br /><br /><div class="error">Błąd serwera! Przepraszamy za niedogodności i prosimy o rejestrację w innym terminie!</div>';
    echo '<br />Informacja developerska: '.$e.'</center>';

  }
}
?>